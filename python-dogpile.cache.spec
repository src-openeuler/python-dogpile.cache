%global _empty_manifest_terminate_build 0
Name:           python-dogpile-cache
Version:        1.3.3
Release:        1
Summary:        A caching front-end based on the Dogpile lock.
License:        MIT
URL:            https://github.com/sqlalchemy/dogpile.cache
Source0:        https://files.pythonhosted.org/packages/81/3b/83ce66995ce658ad63b86f7ca83943c466133108f20edc7056d4e0f41347/dogpile.cache-1.3.3.tar.gz
BuildArch:      noarch

%description
A caching front-end based on the Dogpile lock.

%package -n python3-dogpile-cache
Summary:        A caching front-end based on the Dogpile lock.
Provides:       python-dogpile-cache = %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-dogpile-cache
A caching front-end based on the Dogpile lock.

%package help
Summary:        A caching front-end based on the Dogpile lock.
Provides:       python3-dogpile-cache-doc
%description help
A caching front-end based on the Dogpile lock.

%prep
%autosetup -n dogpile.cache-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} -m pytest || true

%files -n python3-dogpile-cache -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jul 24 2024 liudy <liudingyao@kylinos.cn> - 1.3.3-1
- Update package to version 1.3.3
- Added support for an additional pymemcached client parameter

* Fri Mar 29 2024 wangqiang <wangqiang1@kylinos.cn> - 1.3.2-1
- Update package to version 1.3.2

* Thu Jun 01 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.2.1-1
- Update package to version 1.2.1

* Mon Aug 08 2022 fushanqing <fushanqing@kylinos.cn> - 1.1.8-1
- update to 1.1.8

* Tue May 31 2022 OpenStack_SIG <openstack@openeuler.org> - 1.1.5-1
- Upgrade package python3-dogpile-cache to version 1.1.5

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Package Spec generated

* Sat Nov 21 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
